﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyOpenFileDialog.Test
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog("全部文件(*.*)|*.*|图片文件(*.jpg;*.png;*.gif)|*.jpg;*.png;*.gif|文本文件(*.txt)|*.txt");
            if (dialog.ShowDialog() == true)
            {
                this.tb.Text = dialog.File;
                var files = dialog.Files;
            }
        }
    }
}
