﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace MyOpenFileDialog.Converter
{
    class PathInfo2ForegroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)            {
                PathInfo pi = value as PathInfo;
                if (pi.Type == PathType.Dir)
                {
                    return new SolidColorBrush(Color.FromRgb(218, 165, 32));
                }
                else
                {
                    return new SolidColorBrush(Color.FromRgb(250, 128, 114));
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
