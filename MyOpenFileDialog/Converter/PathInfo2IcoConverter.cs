﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace MyOpenFileDialog.Converter
{
    class PathInfo2IcoConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                PathInfo pi = value as PathInfo;
                switch (pi.Type)
                {
                    case PathType.File:
                        return "\uf016";
                    case PathType.Dir:
                        if (pi.Path == Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory))
                        {
                            return "\uf109";
                        }
                        if (pi.Path == Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments))
                        {
                            return "\uf07c";
                        }
                        if (pi.Path == Environment.GetFolderPath(Environment.SpecialFolder.MyMusic))
                        {
                            return "\uf001";
                        }
                        if (pi.Path == Environment.GetFolderPath(Environment.SpecialFolder.MyPictures))
                        {
                            return "\uf03e";
                        }
                        if (pi.Path == Environment.GetFolderPath(Environment.SpecialFolder.MyVideos))
                        {
                            return "\uf03d";
                        }
                        return "\uf07b";
                    case PathType.Boss:
                        return "\uf108";
                    default:
                        break;
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
