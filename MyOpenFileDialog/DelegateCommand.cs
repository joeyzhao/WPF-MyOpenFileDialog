﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace MyOpenFileDialog
{
    class DelegateCommand : ICommand
    {
        private Action<object> _action;
        private Predicate<object> _canExecuteFunc;

        public DelegateCommand(Action<object> action)
        {
            _action = action;
        }

        public DelegateCommand(Action<object> action, Predicate<object> canExcuteFunc)
        {
            _action = action;
            _canExecuteFunc = canExcuteFunc;
        }

        public bool CanExecute(object parameter)
        {
            if (_canExecuteFunc != null)
            {
                return _canExecuteFunc(parameter);
            }
            return true;
        }

        public void Execute(object parameter)
        {
            _action?.Invoke(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
